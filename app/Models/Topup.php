<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topup extends Model
{
    use HasFactory;

    protected $table = "topup";

    protected $fillable = [
      'resf_code',
      'channel',
      'bank_id',
      'amount'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function bank(){
      return $this->belongsTo("App\Models\Bank","bank_id");
    }
}
