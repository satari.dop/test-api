<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;

    protected $table = "bank";

    protected $fillable = [
      'bank_number',
      'bank_name'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
