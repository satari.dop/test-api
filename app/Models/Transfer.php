<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    use HasFactory;

    protected $table = "transfer";

    protected $fillable = [
      'resf_code',
      'from_bank_id',
      'from_balance',
      'to_bank_id',
      'to_balance',
      'amount'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function from_bank(){
      return $this->belongsTo("App\Models\Bank","from_bank_id");
    }
    public function to_bank(){
      return $this->belongsTo("App\Models\Bank","to_bank_id");
    }
}
