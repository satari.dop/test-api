<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function index()
    {
        try {

            $data = Bank::get();
            $response['data'] = $data;
            $response['success'] = true;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $response['success'] = false;
        }
        return $response;
    }

    
    public function store(Request $request)
    {
        try {
  
          $insert['bank_number'] = $request['bank_number'];
          $insert['bank_name'] = $request['bank_name'];
          $insert['created_at'] = now();
          $insert['updated_at'] = now();
  
          Bank::insert($insert);
  
          $response['message'] = "บันทึกข้อมูลสำเร็จ";
          $response['success'] = true;
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = true;
        }
         
        return $response;
    }

    
    public function show($id)
    {
        try {
  
          $data = Bank::find($id);
  
          if ($data) {
            $response['data'] = $data;
            $response['message'] = "Load successful";
            $response['success'] = true;
          }
          else {
            $response['message'] = "ไม่พบข้อมูลรายการ id => $id";
            $response['success'] = false;
          }
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
        return $response;
    }

    
    public function update(Request $request,$id)
    {
        try {
  
          $data['bank_number'] = $request['bank_number'];
          $data['bank_name'] = $request['bank_name'];
          $insert['updated_at'] = now();
          Bank::where("id",$id)->update($data);
  
          $response['message'] = "แก้ไขข้อมูลสำเร็จ";
          $response['success'] = true;
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
        return $response;
    }

    
    public function destroy($id)
    {
        try {
          $res = Bank::where("id",$id)->delete();
          $response['res'] = $res;
          $response['message'] = "ลบข้อมูลสำเร็จ";
          $response['success'] = true; 
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
  
        return $response;
    }
}
