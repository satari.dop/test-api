<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Transfer;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    public function index()
    {
        try {

            $data = Transfer::with("from_bank")->with("to_bank")->get();

            $response['data'] = $data;
            $response['success'] = true;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $response['success'] = false;
        }
        return $response;
    }

    
    public function store(Request $request)
    {
        try {
  
          $insert['resf_code'] = $request['resf_code'];
          $insert['from_bank_id'] = $request['from_bank_id'];
          $insert['from_balance'] = $request['from_balance'];
          $insert['to_bank_id'] = $request['to_bank_id'];
          $insert['to_balance'] = $request['to_balance'];
          $insert['amount'] = $request['amount'];
          $insert['created_at'] = now();
          $insert['updated_at'] = now();
          Transfer::insert($insert);
  
          $response['message'] = "บันทึกข้อมูลสำเร็จ";
          $response['success'] = true;
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = true;
        }
         
        return $response;
    }

    
    public function show($id)
    {
        try {
  
          $data = Transfer::with("from_bank")->with("to_bank")->find($id);
  
          if ($data) {
            $response['data'] = $data;
            $response['message'] = "Load successful";
            $response['success'] = true;
          }
          else {
            $response['message'] = "ไม่พบข้อมูลรายการ id => $id";
            $response['success'] = false;
          }
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transfer $transfer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transfer  $transfer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transfer $transfer)
    {
        //
    }
}
