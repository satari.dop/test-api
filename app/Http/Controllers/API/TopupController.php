<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Topup;
use Illuminate\Http\Request;

class TopupController extends Controller
{
    public function index()
    {
        try {

            $data = Topup::with("bank")->get();

            $response['data'] = $data;
            $response['success'] = true;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
            $response['success'] = false;
        }
        return $response;
    }

    
    public function store(Request $request)
    {
        try {
  
          $insert['resf_code'] = $this->get_resf();
          $insert['channel'] = $request['channel'];
          $insert['bank_id'] = $request['bank_id'];
          $insert['amount'] = (int)$request['amount'];
          $insert['created_at'] = now();
          $insert['updated_at'] = now();
          $result = Topup::insert($insert);
          
          // $returnData = $result->find($result->id);

          // $response['data'] = $returnData;
          $response['message'] = "บันทึกข้อมูลสำเร็จ";
          $response['success'] = true;
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = true;
        }
         
        return $response;
    }

    
    public function show($id)
    {
        try {
  
          $data = Topup::with("bank")->find($id);
  
          if ($data) {
            $response['data'] = $data;
            $response['message'] = "Load successful";
            $response['success'] = true;
          }
          else {
            $response['message'] = "ไม่พบข้อมูลรายการ id => $id";
            $response['success'] = false;
          }
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
        return $response;
    }

    public function update(Request $request,$id){

        try {
  
          $data['channel'] = $request['channel'];
          $data['bank_id'] = $request['bank_id'];
          $data['amount'] = $request['amount'];
          $data['updated_at'] = now();
  
          Topup::where("id",$id)->update($data);
          
          $response['message'] = "แก้ไขข้อมูลสำเร็จ";
          $response['success'] = true;
  
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
        return $response;
  
    }

    
    public function destroy($id)
    {
        try {
          $res = Topup::where("id",$id)->delete();
          $response['res'] = $res;
          $response['message'] = "ลบข้อมูลสำเร็จ";
          $response['success'] = true; 
        } catch (\Exception $e) {
          $response['message'] = $e->getMessage();
          $response['success'] = false;
        }
  
        return $response;
    }

    private function get_resf()
    {

        $data = Topup::get();
        $count = $data->count();

        if($count > 0){
            $resf = 'T'.date('Ymd').sprintf("%05d",($count+1));
        }else{
            $resf = 'T'.date('Ymd').sprintf("%05d",1);
        }

        return $resf;
    }
}
